function [pay,out_payparam,out_stparam]=payoffx(varargin)
% PAYOFFX generates option payoffs for certain options
%
%   pay = PAYOFFX(x,in_payparam,in_stparam) 
%   generates payoffs of various options from a matrix  x  of numbers which 
%   are assumed to mimic row vectors of normal random variables 
%   with the default parameters
%       stock    = function handle for stock price paths 
%                  (default = GBM)
%       paytype  = type of option
%                  'eurocall', 'europut' = European call/put (default)
%                  'ameancall', 'ameanput' = Arithmetic mean call/put
%                  'lookcall','lookput' = lookback option
%          
%       T     = number of years to maturity (default = 1)
%       K     = strike price (default = 100)
%       r     = interest rate (default = 2%)
%

[out_payparam,out_stparam]=pay_param(varargin{:});
x=out_stparam.x;
[N,d]=size(x);
out_stparam.d=d;
T=out_stparam.T;
r=out_stparam.r;
paytype=out_payparam.paytype;
K=out_payparam.K;
S0=out_stparam.S0;

%% Generate random vectors
temp_stparam=out_stparam; %store parameters
allpaytype={paytype}; %all payoff types, including control variates
npaytype=1; %number of payoffs
ncontrol=0; %number of control variates
if isfield(out_payparam,'control')
   ncontrol=numel(out_payparam.control);
   if ncontrol>0; %are some control variates
      npaytype=npaytype+ncontrol; %total number of payoffs
      allpaytype=[allpaytype out_payparam.control]; %all payoff types updated
   end
end
n=N;
if out_stparam.anti %if there are antithetic variates
   temppay=zeros(2*n,npaytype); %initialize payoff with 2*n rows
else
   temppay=zeros(n,npaytype); %initialize payoff with n rows
end
[stock,out_stparam]=stockpathx(x,temp_stparam);
wh=strcmp(allpaytype,'stockprice');
if any(wh) %final stock price
   temppay(:,wh)=stock(:,d+1)*exp(-r*T); 
end
if any(strcmp(allpaytype,'lookcall')); K=min(stock,[ ],2); end
if any(strcmp(allpaytype,'lookput')); K=max(stock,[ ],2); end
wh=any(strcmp(repmat(allpaytype,6,1),repmat({'eurocall','lookcall',...
   'upincall','upoutcall','downincall','downoutcall'}',1,npaytype)),1);
if any(wh); %call payoff
   temppay(:,wh)=max(stock(:,d+1)-K,0)*exp(-r*T); 
end
wh=any(strcmp(repmat(allpaytype,6,1),repmat({'europut','lookput',...
   'upinput','upoutput','downinput','downoutput'}',1,npaytype)),1);
if any(wh); %put payoff
   temppay(:,wh)=max(K-stock(:,d+1),0)*exp(-r*T);
end
wh=any(strcmp(repmat(allpaytype,2,1), ...
   repmat({'upincall','upinput'}',1,npaytype)),1);
if any(wh); %up and in barrier
   temppay(:,wh)=temppay(:,wh).*any(stock>=out_payparam.barrier,2);
end
wh=any(strcmp(repmat(allpaytype,2,1), ...
   repmat({'upoutcall','upoutput'}',1,npaytype)),1);
if any(wh); %up and in barrier
   temppay(:,wh)=temppay(:,wh).*~any(stock>=out_payparam.barrier,2);
end
wh=any(strcmp(repmat(allpaytype,2,1), ...
   repmat({'downincall','downinput'}',1,npaytype)),1);
if any(wh); %down and in barrier
   temppay(:,wh)=temppay(:,wh).*any(stock<=out_payparam.barrier,2);
end
wh=any(strcmp(repmat(allpaytype,2,1), ...
   repmat({'downoutcall','downoutput'}',1,npaytype)),1);
if any(wh); %down and out barrier
   temppay(:,wh)=temppay(:,wh).*~any(stock<=out_payparam.barrier,2);
end

wh=strcmp(allpaytype,'ameancall');
if any(wh); %arithmetic mean call
   meanstock=mean(stock(:,2:d+1),2);
   temppay(:,wh)=max(meanstock-K,0)*exp(-r*T);
end
wh=strcmp(allpaytype,'ameanput');
if any(wh); %arithmetic mean put
   meanstock=mean(stock(:,2:d+1),2);
   temppay(:,wh)=max(K-meanstock,0)*exp(-r*T);
end
wh=strcmp(allpaytype,'gmeancall');
if any(wh); %geometric mean call
   meanstock=(prod(stock(:,2:d+1),2)).^(1/d);
   temppay(:,wh)=max(meanstock-K,0)*exp(-r*T);
end
wh=strcmp(allpaytype,'gmeanput');
if any(wh); %geometric mean put
   meanstock=(prod(stock(:,2:d+1),2)).^(1/d);
   temppay(:,wh)=max(K-meanstock,0)*exp(-r*T);
end

wh=strcmp(allpaytype,'amerput');
if any(wh)
   basis= @(x) repmat(exp(-x/2),1,3).*[ones(length(x),1) 1-x 1-2*x+x.*x/2];
   putpayoff=max(K-stock,0).*repmat(exp(-r*out_stparam.tnode),N,1); %discounted payoff at each time
   cashflow=putpayoff(:,d+1); %cash flow according to exercise rule
   extime=repmat(d+1,N,1); %exercise time for now
   out_payparam.exbound=zeros(d+1,1); %initialize exercise boundary
   out_payparam.exbound(d+1)=K; %exercise boundary at expiry time
   for jj=d:-1:1 %work backwards from expiry to present
      in=find(putpayoff(:,jj)>0); %which paths are in the money
      if ~isempty(in) %there are some paths in the money
         if jj>1; %not at the start
            regmat=basis(stock(in,jj)/S0); %regression matrix for stock prices
            if out_stparam.meanshift; %weighted regression for importance sampling
               numbasis=size(regmat,2); %number of basis elements
               regwt=sqrt(out_stparam.likeratio(in)); %weights used in regression
               hold=regmat*((repmat(regwt,1,numbasis).*regmat)\ ...
                  (regwt.*cashflow(in))); %regressed value of options in the future
            else %no importance sampling
               hold=regmat*(regmat\cashflow(in)); %regressed value of options in the future
            end
         else %at the start time, cannot do a regression
            if out_stparam.meanshift; %importance sampling with mean shift
               hold=mean(cashflow(in).*out_stparam.likeratio(in));
            else %no importance sampling
               hold=mean(cashflow(in)); 
            end
         end
         shouldex=in(putpayoff(in,jj)>hold); %which paths should be excercised now
         if ~isempty(shouldex); %some paths should be exercise
            cashflow(shouldex)=putpayoff(shouldex,jj); %updated cashflow
            extime(shouldex)=jj; %update
            out_payparam.exbound(jj)=max(stock(shouldex,jj)); 
         end
      end
   end
   temppay(:,wh)=cashflow;
end

if out_stparam.meanshift
   temppay=temppay.*out_stparam.likeratio;
end

if out_stparam.anti %if there are antithetic variates, average them
   pay=(temppay(1:n,:)+temppay(n+1:2*n,:))/2;
else
   pay=temppay;
end

if ncontrol>0 %control variates
   controlprice=exactoptionprice(temp_stparam,out_payparam,...
      out_payparam.control); %get exact prices for the controls
   samplemean=mean(pay,1); %sample means
   beta=(pay(:,2:end)-repmat(samplemean(2:end),N,1))\ ...
      (pay(:,1)-samplemean(1)); %compute beta
   pay=pay(:,1)+(repmat(controlprice,N,1)-pay(:,2:end))*beta;
      %new payoff for control variates
end
end

function [out_payparam,out_stparam]=pay_param(varargin)
% parse the input to the brownmotion.m function

%% Default parameter values
default.T  = 1;% default time horizon
default.S0 = 100;% default initial stock price
default.r = 0.02;% default interest rate
default.sig = 0.5;% default volatility
default.pathtype='GBM';% default path type
default.randtype='IID';% default random numbers
default.disctype='timestep';% default random numbers
default.meanshift = false;% default for antithetic sampling
default.drift = 0;% default for antithetic sampling
default.anti = false;% default for antithetic sampling
default.needcheck = true;% default of whether to check parameter validity
default.paytype = 'eurocall';% default way to get Brownian motion
default.K = 100;% default strike price
default.barrier = 110;% default strike price
default.control = {}; %default control variable

%% Parse inputs
if isempty(varargin) %sample path number not input
    help payoffx
    error('At least x must be specified.')
else
    x=varargin{1};
end

validpaystr=numel(varargin)>1;
if validpaystr
    in_payparam=varargin{2};
    validpaystr=isstruct(in_payparam);
end

validstockstr=validpaystr&&(numel(varargin)>2);
if validstockstr
    in_stparam=varargin{3};
    validstockstr=isstruct(in_stparam);
end

if ~validpaystr
    %only one valid input x, use all the default parameters
    out_payparam.paytype = default.paytype;
    out_payparam.K = default.K;
    out_payparam.barrier = default.barrier;
    out_stparam.control = default.control;
    out_payparam.needcheck=default.needcheck;
else %parse the second input
    p = inputParser;
    addRequired(p,'x',@isnumeric);
    p.StructExpand = true;
    p.KeepUnmatched = true;
    addParameter(p,'paytype',default.paytype,@isstr);
    addParameter(p,'K',default.K,@isnumeric);
    addParameter(p,'barrier',default.barrier,@isnumeric);
    addParameter(p,'control',default.control);
    addParameter(p,'needcheck',default.needcheck,@islogical);
    parse(p,x,in_payparam)
    out_payparam = p.Results;
end

if ~validstockstr
    %only one valid input x, use all the default parameters
    out_stparam.x=x;
    out_stparam.T = default.T;
    out_stparam.S0 = default.S0;
    out_stparam.r = default.r;
    out_stparam.sig=default.sig;
    out_stparam.pathtype=default.randtype;
    out_stparam.randtype=default.randtype;
    out_stparam.disctype=default.disctype;
    out_stparam.meanshift = default.meanshift;
    out_stparam.drift = default.drift;
    out_stparam.anti = default.anti;
    out_stparam.needcheck=default.needcheck;
else %parse in_stparam
    p = inputParser;
    addRequired(p,'x',@isnumeric);
    p.StructExpand = true;
    p.KeepUnmatched = true;
    addParameter(p,'T',default.T,@isnumeric);
    addParameter(p,'S0',default.S0,@isnumeric);
    addParameter(p,'r',default.r,@isnumeric);
    addParameter(p,'sig',default.sig,@isnumeric);
    addParameter(p,'pathtype',default.pathtype,@isstr);
    addParameter(p,'disctype',default.disctype,@isstr);
    addParameter(p,'randtype',default.randtype,@isstr);
    addParameter(p,'meanshift',default.meanshift,@islogical);
    addParameter(p,'drift',default.drift,@isnumeric);
    addParameter(p,'anti',default.anti,@islogical);
    addParameter(p,'needcheck',default.needcheck,@islogical);
    parse(p,x,in_stparam)
    out_stparam = p.Results;
end

%% Check parameter validity
if out_payparam.needcheck
    if ~ischar(out_payparam.paytype)
        warning('Option type should be a character.')
        out_payparam.paytype=[];
    end
    if ~any(strcmpi(out_payparam.paytype,...
            {'stockprice',...
            'eurocall','europut',...
            'ameancall','ameanput'...
            'gmeancall','gmeanput',...
            'lookcall','lookput',...
            'upincall','amerput'}))
        warning(['Payoff type not recognized, using ' default.paytype])
        out_payparam.paytype='eurocall';
    end

    if out_payparam.K <= 0
        warning(['Strike price should be greater than 0, ' ...
            'using ' num2str(default.K)])
        out_payparam.K = default.K;
    end
    
     if out_payparam.barrier <= 0
     warning(['Barrier should be greater than 0, ' ...
         'using ' num2str(default.barrier)])
     out_payparam.K = default.barrier;
    end
out_payparam.needcheck=false;
end


if out_stparam.needcheck

    if out_stparam.T <= 0
        warning(['Time horizon should be greater than 0, ' ...
            'using ' num2str(default.T)])
        out_stparam.T = default.T;
    end

    if out_stparam.S0 < 0
        warning(['Initial stock price should be no less than 0, ' ...
            'using ' num2str(default.S0)])
        out_stparam.S0 = default.S0;
    end

    if out_stparam.r < 0
        warning(['Interest rate should be no less than 0, ' ...
            'using ' num2str(default.r)])
        out_stparam.r = default.r;
    end

    if out_stparam.sig < 0
        warning(['Volatility should be greater than 0, ' ...
            'using ' num2str(default.sig)])
        out_stparam.sig = default.sig;
    end

    if ~ischar(out_stparam.pathtype)
        warning('Stock path type should be a character.')
        out_stparam.type=[];
    end
    if ~any(strcmpi(out_stparam.pathtype,...
            {'GBM'}))
        warning(['Stock path type not recognized, using ' default.pathtype])
        out_stparam.pathtype=default.pathtype;
    end

    if ~ischar(out_stparam.disctype)
        warning('Discretization type should be a character.')
        out_stparam.disctype=[];
    end
    if any(strcmpi(out_stparam.disctype,{'timestep','discrete'}))
        out_stparam.disctype='timestep';
    elseif any(strcmpi(out_stparam.disctype,{'BB','bridge'}))
        out_stparam.disctype='BB';
    else 
        warning(['Discretization type not recognized, using ' default.disctype])
        out_stparam.disctype=default.disctype;
    end
    
    out_stparam.needcheck=false;

end

end  

function b=isposint(a)
%
% ISPOSINT To judge if input is a positive integer or not
b = (ceil(a)==a) && (a>0);
end