function [Bpath,out_sample]=brownmotionx(varargin)
% BROWNMOTIONX generates discrete Brownian motion paths
%
%   Bpath = BROWNMOTIONX(x) 
%   generates paths of a standard Brownian motion on [0,1] using inputs  x
%
%   Bpath = BROWNMOTIONX(x,T,disctype,randtype,needcheck) 
%   generates Brownian motion paths on [0,T].
%   Furthermore,
%       disctype = discretization type
%                  'timestep' = forward time step (default)
%                  'BB' =  Brownian bridge
%          drift = expected value of Brownian motion is drift*t 
%                  (default = 0)
%           anti = generate anti-thetic samples (true or false)
%                  if true, then 2N samples will be returned, the values of 
%                  the second N samples will be minus the values of the
%                  first N samples (default = false)
%      needcheck = logical variable that determines whether parameters
%                  should be checked for validity (default=true)
%   The list of inputs can be truncated, and those missing are set to
%   default values.
%
%   Bpath = BROWNMOTIONX(x,'drift',drift,'T',T,...)
%   generates Brownian motion paths with parameters entered in arbitrary
%   order.
%
%   Bpath = BROWNMOTIONX(x,in_sample) 
%   generates Brownian motion paths with optional parameters entered as
%   fields in the structure in_sample.  If a field is not specified, the
%   default value is used.

%% Parse and check the validity of input parameters
out_sample=bm_param(varargin{:});
x=out_sample.x;
[N,d]=size(x);
out_sample.N=N;
out_sample.d=d;
T=out_sample.T;
disctype=out_sample.disctype;

%% Turn random vectors into sample paths
delta=T/d;
tovT=(1/d:1/d:1);
tvec=tovT*T;
out_sample.tnode=[0 tvec];

if out_sample.meanshift %does not work with antithetics
   shiftmat=repmat(delta*out_sample.drift,N,d);
   x=x+shiftmat;
   out_sample.likeratio=exp(cumsum((shiftmat/2-x).*shiftmat,2));
   out_sample.likeratio=out_sample.likeratio(:,end);
end

switch disctype
    case 'timestep' %forward time step
        Bpath=cumsum([zeros(N,1) x*sqrt(delta)],2);
    case 'BB' %Brownian Bridge
        log2d=log2(d);
        if floor(log2d)==log2d % do it fast
            saw=@(t) 1-abs(1-mod(t,2)); %triangular saw function
            Bpath=zeros(N,d+1);
            srange=[0 2.^(0:log2d)];
            slow=srange(1:log2d+1)+1;
            shi=srange(2:log2d+2);
            snum=diff(srange);
            factor=sqrt(T./[1 4*(shi(1:log2d))]);
            for k=1:log2d+1
                which=repmat(slow(k):shi(k),d/snum(k),1);
                which=which(:)';
                Bpath(:,2:d+1)=Bpath(:,2:d+1)+...
                    x(:,which).*repmat(saw(tovT*shi(k))*factor(k),N,1);
            end
            %keyboard
        else %do it slowly
            hat=@(t) max(1-abs(t),0); %triangular hat function
            tcent=1-net(sobolset(1),d); %van der Corput sequence
            out_sample.tnode=[0 sort(tcent)]; %different nodes
            Bpath=zeros(N,d+1);       
            Bpath(:,2:d+1)=Bpath(:,2:d+1)+...
                x(:,1)*(tovT*sqrt(T));
            if s>1;
                tpowm=2.^(1+floor(log2(1:d-1)));
                for k=2:d;
                    Bpath(:,2:d+1)=Bpath(:,2:d+1)+...
                        x(:,k)*...
                        hat((tovT-tcent(k))*tpowm(k-1))*...
                        (sqrt(T/(tpowm(k-1)*2)));
                end
            end
        end
end
if out_sample.anti %get other half the number of samples
   Bpath=[Bpath;-Bpath(1:N,:)];
end

end

function out_sample=bm_param(varargin)
% parse the input to the brownmotion.m function

%% Default parameter values
default.disctype = 'timestep';% default way to get Brownian motion
default.T  = 1;% default time horizon
default.meanshift = false;% default for antithetic sampling
default.drift = 0;% default for antithetic sampling
default.anti = false;% default for antithetic sampling
default.needcheck = true;% default of whether to check parameter validity

%% Parse inputs
if isempty(varargin) %sample path number not input
    help brownmotionx
    error('At least x must be specified')
else
    x=varargin{1};
end

validvarargin=numel(varargin)>1;
if validvarargin
    in2=varargin{2};
    validvarargin=(isnumeric(in2) || isstruct(in2) ...
        || ischar(in2));
end

if ~validvarargin
    %only one valid input x, use all the default parameters
    out_sample.x=x;
    out_sample.T = default.T;
    out_sample.disctype = default.disctype;
    out_sample.meanshift = default.meanshift;
    out_sample.drift = default.drift;
    out_sample.anti = default.anti;
    out_sample.needcheck = default.needcheck;
else %parse the second input
    p = inputParser;
    addRequired(p,'x',@isnumeric);
    if isnumeric(in2)%if there are multiple inputs with
        %only numeric, they should be put in order.
        addOptional(p,'T',default.T,@isnumeric);
        addOptional(p,'d',default.d,@isnumeric);
        addOptional(p,'disctype',default.disctype,@isstr);
        addOptional(p,'randtype',default.randtype,@isstr);
        addOptional(p,'meanshift',default.meanshift,@islogical);
        addOptional(p,'drift',default.drift,@isnumeric);
        addOptional(p,'anti',default.anti,@islogical);
        addOptional(p,'needcheck',default.needcheck,@islogical);
    else
        if isstruct(in2) %parse input structure
            p.StructExpand = true;
            p.KeepUnmatched = true;
        end
        addParameter(p,'T',default.T,@isnumeric);
        addParameter(p,'disctype',default.disctype,@isstr);
        addParameter(p,'meanshift',default.meanshift,@islogical);
        addParameter(p,'drift',default.drift,@isnumeric);
        addParameter(p,'anti',default.anti,@islogical);
        addParameter(p,'needcheck',default.needcheck,@islogical);
    end
    parse(p,x,varargin{2:end})
    out_sample = p.Results;
end

%% Check parameter validity
if out_sample.needcheck

    if out_sample.T <= 0
        warning(['Time horizon should be greater than 0, ' ...
            'using ' num2str(default.T)])
        out_sample.T = default.T;
    end

    if ~ischar(out_sample.disctype)
        warning('Discretization type should be a character.')
        out_sample.disctype=[];
    end
    if any(strcmpi(out_sample.disctype,{'timestep','discrete'}))
        out_sample.disctype='timestep';
    elseif any(strcmpi(out_sample.disctype,{'BB','bridge'}))
        out_sample.disctype='BB';
    else 
        warning(['Discretization type not recognized, using ' default.disctype])
        out_sample.disctype=default.disctype;
    end

    if any(strcmp(out_sample.disctype,{'BB','KL'})) ...
            && ~isposint(out_sample.s) % number of terms in expansion should be an integer
        warning(['The number of terms in the expansion should be a positive integer, '...
            'using ' int2str(default.s)])
        out_sample.s = default.s;
    end

end 
end  

function b=isposint(a)
%
% ISPOSINT To judge if input is a positive integer or not
b = (ceil(a)==a) && (a>0);
end
